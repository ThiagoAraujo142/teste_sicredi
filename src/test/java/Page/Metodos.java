package Page;

import static org.junit.Assert.assertEquals;



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;


public class Metodos {
	

	WebDriver driver;

	public void abrirNavegador(String site) {

		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(site);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	public void clicar(By elemento) {

		driver.findElement(elemento).click();

	}

	public void comboSelect(By elemento, String opcao) {

		WebElement element = driver.findElement(elemento);
		Select combo = new Select(element);
		combo.selectByValue(opcao);

	}

	public void escrever(By elemento, String texto) {
		driver.findElement(elemento).sendKeys(texto);

	}

	public void validacao(String texto, By elemento) {
		String txt = driver.findElement(elemento).getText();
		assertEquals(texto, txt);

	}
	
	public void pausa(int tempo) throws InterruptedException {
		Thread.sleep(tempo);

	}

	public void fecharNavegador() {

		driver.quit();
	}
	

}
