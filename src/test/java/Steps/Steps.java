package Steps;

import java.io.IOException;

import ElementosWeb.Elementos;
import Page.Metodos;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;



public class Steps {
	
	Metodos metodos = new Metodos();
	Elementos elemento = new Elementos();

	
	@Dado("que esteja no site")
	public void que_esteja_no_site() {

		metodos.abrirNavegador("https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap");

	}

	@Quando("eu preencho as informacoes de cadastro e salvo")
	public void eu_preencho_as_informacoes_de_cadastro_e_salvo() {
		metodos.clicar(elemento.getButton());
		metodos.comboSelect(elemento.getButton(), "/v1.x/demo/my_boss_is_in_a_hurry/bootstrap-v4");
		metodos.clicar(elemento.getAdd());
		metodos.escrever(elemento.getName(), "Teste Sicredi");
		metodos.escrever(elemento.getLastName(), "Teste");
		metodos.escrever(elemento.getContactName(), "Mauricio Avila");
		metodos.escrever(elemento.getPhone(), "51 9999-9999");
		metodos.escrever(elemento.getAddDressLine1(), "Av Assis Brasil, 3970");
		metodos.escrever(elemento.getAddDressLine2(), "Torre D");
		metodos.escrever(elemento.getCity(), "Porto Alegre");
		metodos.escrever(elemento.getState(), "RS");
		metodos.escrever(elemento.getPostAlCode(), "91000-000");
		metodos.escrever(elemento.getCountry(), "Brasil");
		metodos.escrever(elemento.getSalesNumber(), "999999");
		metodos.escrever(elemento.getCreditLimit(), "200");
		metodos.clicar(elemento.getSalve());
	}

	@Entao("valido as informacoes")
	public void valido_as_informacoes()throws InterruptedException, IOException {
		metodos.pausa(2000);
		metodos.validacao("Your data has been successfully stored into the database. Edit Record or Go back to list",
				elemento.getMensage());
		metodos.fecharNavegador();

	}

}
