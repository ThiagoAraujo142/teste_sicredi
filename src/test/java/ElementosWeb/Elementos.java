package ElementosWeb;

import org.openqa.selenium.By;

public class Elementos {

	private By button = By.id("switch-version-select");
	private By add = By.xpath("//*[@id=\"gcrud-search-form\"]/div[1]/div[1]/a");
	private By name = By.name("customerName");
	private By lastName = By.name("contactLastName");
	private By contactName = By.name("contactFirstName");
	private By phone = By.name("phone");
	private By addDressLine1 = By.name("addressLine1");
	private By addDressLine2 = By.name("addressLine2");
	private By city = By.name("city");
	private By state = By.name("state");
	private By postAlCode = By.name("postalCode");
	private By country = By.name("country");
	private By salesNumber = By.name("salesRepEmployeeNumber");
	private By creditLimit = By.name("creditLimit");
	private By salve = By.id("form-button-save");
	private By mensage = By.id("report-success");
	 
	
	public By getButton() {
		return button;
	}
	public By getAdd() {
		return add;
	}
	public By getName() {
		return name;
	}
	public By getLastName() {
		return lastName;
	}
	public By getContactName() {
		return contactName;
	}
	public By getPhone() {
		return phone;
	}
	public By getAddDressLine1() {
		return addDressLine1;
	}
	public By getAddDressLine2() {
		return addDressLine2;
	}
	public By getCity() {
		return city;
	}
	public By getState() {
		return state;
	}
	public By getPostAlCode() {
		return postAlCode;
	}
	public By getCountry() {
		return country;
	}
	public By getSalesNumber() {
		return salesNumber;
	}
	public By getCreditLimit() {
		return creditLimit;
	}
	public By getSalve() {
		return salve;
	}
	public By getMensage() {
		return mensage;
	}
}
